#!/bin/bash
OLDPATH="$PATH"
WHEREAMI="`pwd`"

export PATH=$WHEREAMI/node_binaries/bin:"$PATH"
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
CLIWALLET="$HYBRIXD/cli-wallet"
DIST="$CLIWALLET/dist"

VERSION="v"$(cat package.json | grep version | cut -d'"' -f4)
echo " [i] Version $VERSION"

cd $DIST
echo " [.] Compressing distribution into dist.zip ..."
zip -qr "../dist.zip" .
echo " [.] Compressing distribution into dist.tar.gz ..."
tar -zcf "../dist.tar.gz" .
cd ..

TARGETHOST="service@download.hybrix.io"
TARGETPATH="~/public_html/releases/cli-wallet"
ssh $TARGETHOST "mkdir -p $TARGETPATH/latest ; mkdir -p $TARGETPATH/$VERSION"

echo " [.] Uploading distribution to $VERSION ..."
TARGETFILEA="hybrix.cli-wallet.$VERSION.zip"
scp dist.zip $TARGETHOST:$TARGETPATH/$VERSION/$TARGETFILEA

TARGETFILEB="hybrix.cli-wallet.$VERSION.tar.gz"
scp dist.tar.gz $TARGETHOST:$TARGETPATH/$VERSION/$TARGETFILEB

echo " [.] Setting $VERSION as latest..."
ssh $TARGETHOST "cp $TARGETPATH/$VERSION/$TARGETFILEA $TARGETPATH/latest/hybrix.cli-wallet.latest.zip ; cp $TARGETPATH/$VERSION/$TARGETFILEB $TARGETPATH/latest/hybrix.cli-wallet.latest.tar.gz"

echo " [.] Cleaning up"
rm "$CLIWALLET/dist.zip"
rm "$CLIWALLET/dist.tar.gz"

export PATH="$OLDPATH"
cd "$WHEREAMI"


echo " [i] All done!   Go upgrade live instances. Then let the world know."
echo "                 To fire off social media: ./socialsRelease.sh"
echo
