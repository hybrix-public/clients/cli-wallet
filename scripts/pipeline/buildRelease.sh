#!/bin/bash
OLDPATH="$PATH"
WHEREAMI="`pwd`"

export PATH=$WHEREAMI/node_binaries/bin:"$PATH"
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"
CLIWALLET="$HYBRIXD/cli-wallet"
DIST="$CLIWALLET/dist"

echo " [.] Clean up"
rm -rf $DIST
mkdir $DIST

echo " [.] Compile"
sh ./scripts/npm/compile.sh

echo " [i] All done! Now go and upload the release."

export PATH="$OLDPATH"
cd "$WHEREAMI"
