const {getLogin} = require('../../lib/setup');

exports.args = 1;
exports.host = 'allocation';
exports.description = 'Dispute a deal (already disputed deals are escalated) [argument: dealID]';

exports.dispute = (ops) => (dealID) => [
  getLogin(ops, {...this, host: ''}), 'session',
  {query: '/e/swap/deal/dispute/' + dealID}, 'rout'
];
