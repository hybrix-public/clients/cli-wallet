This module makes it possible to swap cryptocurrencies and tokens across
ledgers. This is done by requesting a proposal from the swap matcher,
and then accepting the deal offered.

The easiest way to use this is by issuing the swap command, which then
results in the swap process being performed automatically.

 Use this module by passing the command line option:
  -M deal <command>
