const {getSignatureSteps} = require('./allocationSignature');

exports.args = 0;
exports.host = 'allocation';
exports.description = 'Get contact target for receiving notifications';

exports.contactGet = (ops) => (email) => [
  ...getSignatureSteps(ops, this, 'contactAccount', []),
  ({accountID, signature}) => ({query: '/e/swap/allocation/account/contact/' + accountID + '/' + signature}), 'rout',
];
