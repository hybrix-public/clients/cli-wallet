const {getSignatureSteps} = require('./allocationSignature');

const {MINIMAL_DEPOSIT} = require('./create');
exports.args = 3;
exports.host = 'allocation';
exports.description = 'Set or create an allocation pair [argument: base] [argument: symbol] [argument: feePercent]'; // DISABLED: [argument: riskPercent] [argument: balanceWeightPercent]';

exports.pairSet = (ops) => (fromSymbol, toSymbol, feePercent, riskPercent, balanceWeightPercent) => [
  ...getSignatureSteps(ops, this, 'securityReserveAccount', ['details']),
  ({accountID, signature}) => ({query: '/e/swap/allocation/account/securityReserve/' + accountID + '/details/' + signature}), 'rout',

  details => ({condition: details.balance >= MINIMAL_DEPOSIT, message: `A minimal security deposit of ${MINIMAL_DEPOSIT} ${details.symbol.toUpperCase()} is required to setup this pair, currently only ${details.balance} ${details.symbol.toUpperCase()} is available.`}), 'assert',

  ...getSignatureSteps(ops, this, 'setPair', [fromSymbol, toSymbol, feePercent, riskPercent, balanceWeightPercent]),
  ({accountID, signature}) => ({query: '/e/swap/allocation/pair/set/' + accountID + '/' + fromSymbol + '/' + toSymbol + '/' + feePercent + '/' + riskPercent + '/' + balanceWeightPercent + '/' + signature}), 'rout'
];
