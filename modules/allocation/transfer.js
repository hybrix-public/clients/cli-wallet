const {fail, getLogin, VIRTUAL_SYMBOL_PREFIX} = require('../../lib/setup');
const {getSignatureSteps} = require('./allocationSignature');

exports.host = 'allocation';
exports.args = 3;
exports.description = 'Transfer an amount from an allocation account [argument: symbol] [argument: amount] [argument: target]';

const transferAction = (ops, symbol, amount, target) => {
  return [
    {
      'rebalance' : [
        ...getSignatureSteps(ops, this, 'rebalancePair', [symbol, `-${amount}`]),
        ({accountID, signature}) => ({query: `/e/swap/allocation/pair/rebalance/${accountID}/${symbol}/-${amount}/${signature}`}), 'rout'
      ],
      'txid' : [
        getLogin(ops, this), 'session', // initialize with allocation session
        {symbol, amount, target}, 'transaction'
      ]
    }, 'parallel',
    result => { return { action: 'transfer', 'rebalance': result.rebalance?result.rebalance:false, 'txid': result.txid?result.txid:false }; }
  ];
}

exports.transfer = (ops) => (symbol, amount, target) => {
  if (symbol.charAt(0)===VIRTUAL_SYMBOL_PREFIX) {
    return fail(`Virtual assets cannot be transferred since they only mirror an actual balance outside of the hybrix ecosystem.`);
  } else {
    return transferAction(ops, symbol, amount, target);
  }
}

