const {fail, VIRTUAL_SYMBOL_PREFIX} = require('../../lib/setup');
const {getSignatureSteps} = require('./allocationSignature');

exports.args = 1;
exports.host = 'allocation';
exports.description = 'Get the balance of an allocation account [argument: symbol]';

const balanceReal = (ops, symbol) => {
  return [
    {symbol}, 'getBalance'
  ];
}

const balanceVirtual = (ops, symbol) => {
  return [
    ...getSignatureSteps(ops, this, 'balanceAccount', [symbol]),
    ({accountID, signature}) => ({query: `/e/swap/allocation/account/balance/${accountID}/${symbol}/${signature}`}), 'rout'
  ];
}

exports.balance = (ops) => (symbol) => {
  if (symbol.charAt(0)===VIRTUAL_SYMBOL_PREFIX) {
    return balanceVirtual(ops, symbol);
  } else {
    return balanceReal(ops, symbol);
  }
}
