const {fail, getLogin,VIRTUAL_SYMBOL_PREFIX} = require('../../lib/setup');
const {getSignatureSteps} = require('./allocationSignature');

exports.host = 'allocation';
exports.args = 2;
exports.description = 'Deposit an amount into an allocation account [argument: symbol] [argument: amount]';

const depositReal = (ops, symbol, amount) => {
  const passData = {};
  return [
    {
      'rebalance' : [
        ...getSignatureSteps(ops, this, 'rebalancePair', [symbol, `${amount}`]),
        ({accountID, signature}) => ({query: `/e/swap/allocation/pair/rebalance/${accountID}/${symbol}/${amount}/${signature}`}), 'rout'
      ],
      'txid' : [
        {symbol}, 'getAddress', // get allocation address
        address => { passData.address = address; },
        getLogin(ops, {...this, host: ''}), 'session', // initialize with regular non-allocation session
        () => { return {symbol, amount, target:passData.address }; }, 'transaction'
      ]
    }, 'parallel',
    result => { return { action: 'deposit', 'rebalance': result.rebalance?result.rebalance:false, 'txid': result.txid?result.txid:false }; }    
  ];
}

const depositVirtual = (ops, symbol, amount) => {
  return [
    ...getSignatureSteps(ops, this, 'rebalancePair', [symbol, `${amount}`]),
    ({accountID, signature}) => ({query: `/e/swap/allocation/pair/rebalance/${accountID}/${symbol}/${amount}/${signature}`}), 'rout',
    result => { return { action: 'deposit', 'rebalance': result, 'txid': null }; }
  ];
}

exports.deposit = (ops) => (symbol, amount) => {
  if (symbol.charAt(0)===VIRTUAL_SYMBOL_PREFIX) {
    return depositVirtual(ops, symbol, amount);
  } else {
    return depositReal(ops, symbol, amount);
  }
}
