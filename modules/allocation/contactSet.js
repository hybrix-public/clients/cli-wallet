const {getSignatureSteps} = require('./allocationSignature');

exports.args = 1;
exports.host = 'allocation';
exports.description = 'Set contact target for receiving notifications [argument: email]';

exports.contactSet = (ops) => (email) => [
  ...getSignatureSteps(ops, this, 'setContactAccount', [email]),
  ({accountID, signature}) => ({query: '/e/swap/allocation/account/setContact/' + accountID + '/' + email + '/' + signature}), 'rout',
  (result) => { return `Contact set to: ${result}`; }
];
