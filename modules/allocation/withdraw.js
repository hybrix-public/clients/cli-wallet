const {fail, getLogin, VIRTUAL_SYMBOL_PREFIX} = require('../../lib/setup');
const {getSignatureSteps} = require('./allocationSignature');

exports.host = 'allocation';
exports.args = 2;
exports.description = 'Withdraw an amount from an allocation account [argument: symbol] [argument: amount]';

const withdrawReal = (ops, symbol, amount) => {
  const passData = {};
  return [
    {
      'rebalance' : [
        ...getSignatureSteps(ops, this, 'rebalancePair', [symbol, `-${amount}`]),
        ({accountID, signature}) => ({query: `/e/swap/allocation/pair/rebalance/${accountID}${symbol}/-${amount}/${signature}`}), 'rout'
      ],
      'txid' : [
        getLogin(ops, {...this, host: ''}), 'session', // switch to regular non-allocation session to get address!
        {symbol}, 'getAddress', // get regular address
        address => { passData.address = address; },
        getLogin(ops, this), 'session', // initialize with allocation session
        () => { return {symbol, amount, target:passData.address }; }, 'transaction'
      ]
    }, 'parallel',
    result => { return { action: 'withdraw', 'rebalance': result.rebalance?result.rebalance:false, 'txid': result.txid?result.txid:false }; }
  ];
}

const withdrawVirtual = (ops, symbol, amount) => {
  return [
    ...getSignatureSteps(ops, this, 'rebalancePair', [symbol, `-${amount}`]),
    ({accountID, signature}) => ({query: `/e/swap/allocation/pair/rebalance/${accountID}/${symbol}/-${amount}/${signature}`}), 'rout'
    result => { return { action: 'withdraw', 'rebalance': result, 'txid': null }; }
  ];
}

exports.withdraw = (ops) => (symbol, amount) => {
  if (symbol.charAt(0)===VIRTUAL_SYMBOL_PREFIX) {
    return withdrawVirtual(ops, symbol, amount);
  } else {
    return withdrawReal(ops, symbol, amount);
  }
}
