const {getSignatureSteps} = require('./allocationSignature');

exports.args = 2;
exports.host = 'allocation';
exports.description = 'Delete an allocation pair, making it unavailable [argument: base] [argument: symbol]';

exports.pairDelete = (ops) => (fromBase, toSymbol) => [
  ...getSignatureSteps(ops, this, 'deletePair', [fromBase, toSymbol]),
  ({accountID, signature}) => ({query: '/e/swap/allocation/pair/delete/' + accountID + '/' + fromBase + '/' + toSymbol + '/' + signature}), 'rout'
];
